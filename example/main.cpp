#include <pickle/serialize.hpp>

#include "proto/cpp/example.pb.h"

int main() {
  std::string bytes;

  {
    proto::example::TestMessage message;

    message.set_str("Hello");
    message.set_flag(true);
    message.set_number(42);

    bytes = pickle::Serialize(message);
  }

  {
    proto::example::TestMessage message;

    bool ok = pickle::Deserialize(bytes, &message);
    assert(ok);

    std::cout << "name = " << message.str()
        << ", flag = " << message.flag()
        << ", number = " << message.number()
        << std::endl;
  }

  return 0;
}