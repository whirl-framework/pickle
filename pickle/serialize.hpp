#pragma once

#include <pickle/format/binary.hpp>
#include <pickle/format/json.hpp>

namespace pickle {

#if defined(PICKLE_JSON)

using Format = detail::JsonFormat;

#else

using Format = detail::BinaryFormat;

#endif

template <typename TMessage>
std::string Serialize(const TMessage& message) {
  return Format::Serialize(message);
}

template <typename TMessage>
bool Deserialize(const std::string& bytes, TMessage* message) {
  return Format::Deserialize(bytes, message);
}

inline bool IsBinaryFormat() {
  return Format::IsBinary();
}

}  // namespace pickle
