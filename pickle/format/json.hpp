#pragma once

#include <google/protobuf/util/json_util.h>

#include <string>

namespace pickle {

namespace detail {

struct JsonFormat {
  template <typename TMessage>
  static std::string Serialize(const TMessage& message) {
    std::string json;
    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace = true;
    options.always_print_primitive_fields = true;
    options.preserve_proto_field_names = true;
    google::protobuf::util::MessageToJsonString(message, &json, options);
    return json;
  }

  template <typename TMessage>
  static bool Deserialize(const std::string& json, TMessage* message) {
    return google::protobuf::util::JsonStringToMessage(json, message).ok();
  }

  inline static bool IsBinary() {
    return false;
  }
};

}  // namespace detail

}  // namespace pickle
