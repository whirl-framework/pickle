#pragma once

#include <string>

namespace pickle {

namespace detail {

struct BinaryFormat {
  template <typename TMessage>
  static std::string Serialize(const TMessage& message) {
    return message.SerializeAsString();
  }

  template <typename TMessage>
  static bool Deserialize(const std::string& bytes, TMessage* message) {
    return message->ParseFromString(bytes);
  }

  inline static bool IsBinary() {
    return true;
  }
};

}  // namespace detail

}  // namespace pickle
